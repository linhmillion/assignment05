﻿using Microsoft.Data.SqlClient;

namespace Assignment05
{
    public class SQLHelper
    {
        //private SqlConnection sqlConnection;
        private string connectionString;

        public SQLHelper(/*SqlConnection sqlCnnection,*/string connectionString)
        {
            //this.sqlConnection = sqlConnection;
            this.connectionString = connectionString;
        }

        public void HandleAddCustomer()
        {
            Console.WriteLine("Enter First Name:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter Last Name:");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter Country:");
            string country = Console.ReadLine();
            Console.WriteLine("Enter Postal Code:");
            string postalCode = Console.ReadLine();
            Console.WriteLine("Enter Phone Number:");
            string phoneNumber = Console.ReadLine();
            Console.WriteLine("Enter Email:");
            string email = Console.ReadLine();

            AddCustomer(firstName, lastName, country, postalCode, phoneNumber, email);
        }

        public void HandleGetCustomer(string action)
        {
            switch(action)
            {
                case "all":
                    GetAllCustomers(0);
                    break;
                case "id":
                    Console.WriteLine("Enter Id:");
                    string input = Console.ReadLine();
                    Int32.TryParse(input, out int id);
                    GetCustomerById(id);
                    break;
                case "name":
                    Console.WriteLine("Enter Name:");
                    string name = Console.ReadLine();
                    GetCustomerByName(name);
                    break;
                case "page":
                    GetAllCustomers(10);
                    break;

                default:
                    Console.WriteLine("Unknown Action");
                    break;
            }
        }







        public void GetCustomerByName(string name)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                 
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }

                try
                {
                    string readSQL = $"SELECT * FROM Customer WHERE FirstName LIKE \'{name}%\'";

                    using (SqlCommand command = new SqlCommand(readSQL, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            try
                            {
                                reader.GetValue(0);
                            }
                            catch
                            {
                                Console.WriteLine($"Customer with name {name} does not exist.");
                            }

                            while (reader.Read())
                            {
                                Console.WriteLine($"ID {reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}\n" +
                                    $"Country: {reader.GetValue(7)}\n" +
                                    $"Postal Code: {reader.GetValue(8)}\n" +
                                    $"Phone No.: {reader.GetValue(9)}\n" +
                                    $"Email: {reader.GetValue(11)}\n");
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }





        public void CustomerMostPopularGenre()
        {
            Console.WriteLine("Genre....");
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                    //Console.WriteLine("Connected");
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }

                Console.WriteLine("Enter Id:");
                string input = Console.ReadLine();
                Int32.TryParse(input, out int id);

                try
                {
                    string readSQL = 
                        $"SELECT Genre.Name, COUNT(Genre.Name) FROM Customer\n" +
                        $"INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId\n" +
                        $"INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId\n" +
                        $"INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId\n" +
                        $"INNER JOIN Genre ON Genre.GenreId = Track.GenreId\n" +
                        $"WHERE Customer.CustomerId = {id}\n" +
                        $"GROUP BY Customer.CustomerId, Genre.Name\n" +
                        $"ORDER BY COUNT(Genre.Name) DESC";

                    using (SqlCommand command = new SqlCommand(readSQL, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine($"Genre {reader.GetValue(0)}: {reader.GetValue(1)}");
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }

        public void HandleUpdateCustomer()
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                    //Console.WriteLine("Connected");
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }

                Console.WriteLine("Enter Id:");
                string input = Console.ReadLine();
                Int32.TryParse(input, out int id);

                Console.WriteLine("Enter Column Name:");
                string column = Console.ReadLine();

                Console.WriteLine("Enter Update value:");
                string updatedValue = Console.ReadLine();

                try
                {
                    string updateSQL = $"UPDATE Customer SET {column}=\'{updatedValue}\' WHERE CustomerID={id}";


                    //Console.WriteLine(updateSQL);
                    using (SqlCommand command = new SqlCommand(updateSQL, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }

        void AddCustomer(string firstName, string lastName, string country, string postalCode, string phoneNumber, string email)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                    //Console.WriteLine("Connected");
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Could not connect to the the database: {e.Message}");
                }

                string writeSQL = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

                try
                {
                    using (SqlCommand command = new SqlCommand(writeSQL, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", firstName);
                        command.Parameters.AddWithValue("@LastName", lastName);
                        command.Parameters.AddWithValue("@Country", country);
                        command.Parameters.AddWithValue("@PostalCode", postalCode);
                        command.Parameters.AddWithValue("@Phone", phoneNumber);
                        command.Parameters.AddWithValue("@Email", email);

                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }




        public void CustomerCountryCount()
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                    //Console.WriteLine("Connected");
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }

                try
                {
                    string readSQL = "SELECT COUNT(CustomerID), Country FROM Customer WHERE Country IS NOT NULL GROUP BY Country ORDER BY COUNT(CustomerID) DESC";

                    using (SqlCommand command = new SqlCommand(readSQL, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine($"Country {reader.GetValue(1)}: {reader.GetValue(0)}");
                       
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }



        public void CustomerHighestSpender()
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                    //Console.WriteLine("Connected");
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }

                try
                {
                    string readSQL = "SELECT CustomerId, SUM(Total) FROM Invoice GROUP BY CustomerId ORDER BY SUM(Total) DESC";

                    using (SqlCommand command = new SqlCommand(readSQL, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine($"Customer {reader.GetValue(0)}: {reader.GetValue(1)}");

                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }




        void GetAllCustomers(int limit)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                    //Console.WriteLine("Connected");
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }

                try
                {
                    string readSQL = $"SELECT * FROM Customer";
                    if (limit != 0)
                       readSQL = $"SELECT TOP {limit} * FROM Customer;";
                    Console.WriteLine(readSQL);
                    using (SqlCommand command = new SqlCommand(readSQL, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine($"ID {reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}\n" +
                                    $"Country: {reader.GetValue(7)}\n" +
                                    $"Postal Code: {reader.GetValue(8)}\n" +
                                    $"Phone No.: {reader.GetValue(9)}\n" +
                                    $"Email: {reader.GetValue(11)}\n");
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }

        void GetCustomerById(int id)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                try
                {
                    connection.Open();
                    //Console.WriteLine("Connected");
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }

                try
                {
                    string readSQL = $"SELECT * FROM Customer WHERE CustomerID={id}";

                    using (SqlCommand command = new SqlCommand(readSQL, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            try
                            {
                                reader.GetValue(0);
                            }
                            catch
                            {
                                Console.WriteLine($"Customer with Id {id} does not exist.");
                            }

                            while (reader.Read())
                            {
                                Console.WriteLine($"ID {reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}\n" +
                                    $"Country: {reader.GetValue(7)}\n" +
                                    $"Postal Code: {reader.GetValue(8)}\n" +
                                    $"Phone No.: {reader.GetValue(9)}\n" +
                                    $"Email: {reader.GetValue(11)}\n");
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Something went wrong with the database: {e.Message}");
                }
                connection.Close();
            }
        }
    }
}
