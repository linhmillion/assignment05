CREATE TABLE Superhero (
    ID int IDENTITY(1,1) PRIMARY KEY,
    HeroName NVARCHAR (200),
    Alias NVARCHAR (200),
    Origin NVARCHAR (500)
);


CREATE TABLE Assistant (
    AssistantID int IDENTITY(1,1) PRIMARY KEY,
    AssistantName NVARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE Power (
    PowerID int IDENTITY(1,1) PRIMARY KEY,
    PowerName NVARCHAR (200) NOT NULL,
    PowerDescription NVARCHAR (600)
);

-- exec sp_rename 'Power.PowweID', 'PowerId'
