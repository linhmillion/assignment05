-- ALTER TABLE dbo.Assistant
-- ADD CONSTRAINT FK_HeroAssistant

-- FOREIGN KEY (heroID) REFERENCES dbo.SupperHero(ID);



-- CREATE TABLE [dbo].[Assistant] (
--     [AssistantID]   INT            IDENTITY (1, 1) NOT NULL,
--     [AssistantName] NVARCHAR (100) NOT NULL,
--     [HeroId]        INT            NULL,
--     PRIMARY KEY CLUSTERED ([AssistantID] ASC),
--     FOREIGN KEY ([HeroId]) REFERENCES [dbo].[Supperhero] ([ID])
-- );




ALTER TABLE Assistant
    ADD HeroId INT
    REFERENCES Superhero(ID); 