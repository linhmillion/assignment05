-- 01_dbCreate.sql
CREATE DATABASE SuperheroDB

-- 02_tableCreate.sql
CREATE TABLE Assistant
(
	AssistantID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	AssistantName varchar(255)
);

CREATE TABLE SuperPower
(
	SuperPowerID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	SuperPowerName varchar(255),
	SuperPowerDescription varchar(255)
);

CREATE TABLE Superhero
(
	HeroID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	HeroAlias varchar(255) NOT NULL,
	HeroOrigin varchar(255),
);

-- 03_relationshipSuperheroAssistant.sql
-- TODO add one-to-many and many-to-many relationships for Assistants and Powers

--ALTER TABLE Superhero
--ADD AssistantID int FOREIGN KEY REFERENCES Assistant(AssistantID)

-- 04_relationshipSuperheroPower.sql

--ALTER TABLE Superhero
--ADD SuperPowerID int FOREIGN KEY REFERENCES SuperPower(SuperPowerID)

-- 05_insertSuperheroes.sql

INSERT INTO Superhero(HeroAlias, HeroOrigin)
VALUES('Testman1', 'Test1')
INSERT INTO Superhero(HeroAlias, HeroOrigin)
VALUES('Testman2', 'Test2')
INSERT INTO Superhero(HeroAlias, HeroOrigin)
VALUES('Testman3', 'Test3')

-- 06_insertAssistants.sql

INSERT INTO Assistant(AssistantName)
VALUES('Testboy1')
INSERT INTO Assistant(AssistantName)
VALUES('Testboy2')
INSERT INTO Assistant(AssistantName)
VALUES('Testboy3')

-- 07_insertSuperPowers.sql

INSERT INTO SuperPower(SuperPowerName, SuperPowerDescription)
VALUES('TestBlast', 'Blasts the enemy')
INSERT INTO SuperPower(SuperPowerName, SuperPowerDescription)
VALUES('TestKick', 'Kicks the enemy')
INSERT INTO SuperPower(SuperPowerName, SuperPowerDescription)
VALUES('TestBomb', 'Bombs the enemy')

-- 08_updateSuperhero.sql

UPDATE Superhero
SET HeroAlias='Bob'
WHERE HeroID = 1

UPDATE Superhero
SET HeroOrigin='Pluto'
WHERE HeroID = 3

-- 09_deleteAssistant.sql

DELETE FROM Assistant WHERE AssistantID = 3

-- VIEW TABLE

SELECT * FROM Superhero
SELECT * FROM Assistant
SELECT * FROM SuperPower