﻿using Microsoft.Data.SqlClient;

namespace Assignment05
{
    public class Assignment05
    {
        public static void Main(string[] args)
        {
            string connectionString = "";

            //connectionString = InitConnectionString();

            SQLHelper helper = new(connectionString);

            bool running = true;
            while (running)
            {
                Console.WriteLine("Your action (1 = add, 2 = get all, 3 = get by id, 4 = get by name, 5 = get by page\n" +
                    "6 = update by id, 7 = group by country, 8 = highest spenders, 9 = genres, 10 = exit):");
                string input = Console.ReadLine();
                Int32.TryParse(input, out int choice);
                switch (choice)
                {
                    case 1:
                        helper.HandleAddCustomer();
                        break;
                    case 2:
                        helper.HandleGetCustomer("all");
                        break;
                    case 3:
                        helper.HandleGetCustomer("id");
                        break;
                    case 4:
                        helper.HandleGetCustomer("name");
                        break;
                    case 5:
                        helper.HandleGetCustomer("page");
                        break;
                    case 6:
                        helper.HandleUpdateCustomer();
                        break;
                    case 7:
                        helper.CustomerCountryCount();
                        break;
                    case 8:
                        helper.CustomerHighestSpender();
                        break;
                    case 9:
                        helper.CustomerMostPopularGenre();
                        break;
                    case 10:
                        running = false;
                        break;
                    default:
                        Console.WriteLine("Please enter a valid Number");
                        break;
                }
            }
        }

        static string InitConnectionString()
        {
            // Create Connection String
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            builder.DataSource = "LAPTOP-UPMG9ILS/SQLEXPRESS"; //URL as instantiated in SSMS
            builder.InitialCatalog = "Chinook"; //folder/filename
            builder.IntegratedSecurity = true;
            builder.Encrypt = false; //trying to encrypt the message but it isn't necessary

            return builder.ConnectionString;
        }
    }
}